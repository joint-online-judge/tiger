module runner

go 1.17

require github.com/opencontainers/runtime-spec v1.0.2

require github.com/containerd/cgroups v1.0.3

require github.com/vmihailenco/msgpack/v5 v5.3.5

require (
	github.com/coreos/go-systemd/v22 v22.3.2 // indirect
	github.com/docker/go-units v0.4.0 // indirect
	github.com/godbus/dbus/v5 v5.0.4 // indirect
	github.com/gogo/protobuf v1.3.2 // indirect
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
	golang.org/x/sys v0.0.0-20210510120138-977fb7262007 // indirect
)
